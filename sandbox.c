#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <ucontext.h>

void readString(char *s, int r);

void sighandler(int sig, siginfo_t *siginfo, void *ucp)
{
  ucontext_t *uctx = (ucontext_t*)ucp;
  static void *ret = NULL;
  void *curr = siginfo->si_addr;

#ifdef DEBUG
  printf("SEGV at %p, %p, %p, %p\n", curr, ucp, uctx->uc_mcontext.gregs[15], ret);
#endif

  if (curr == readString) {
    /* ret should be NULL! */
    ret = (void*)(*(unsigned long*)uctx->uc_mcontext.gregs[15]);

    mprotect((void*)(((unsigned long)curr)&0xfffffffffffff000), 1,
             PROT_READ|PROT_WRITE|PROT_EXEC);
    mprotect((void*)(((unsigned long)ret)&0xfffffffffffff000), 1,
             PROT_NONE);
  } else {
    if (ret != NULL && siginfo->si_addr == ret) {
      mprotect((void*)(((unsigned long)ret)&0xfffffffffffff000), 1,
               PROT_READ|PROT_WRITE|PROT_EXEC);
    } else {
      fprintf(stdout, "malicious buffer overflow detected, will exit!\n");
      exit(1);
    }
  }
}

void init_sandbox(void)
{
  struct sigaction act;

  sigemptyset(&act.sa_mask);
  act.sa_handler = NULL;
  act.sa_sigaction = sighandler;
  act.sa_flags = SA_SIGINFO | SA_RESTART;
  if (sigaction(SIGSEGV, &act, NULL)) {
    fprintf(stderr, "signal handler couldn't be registered!\n");
    exit(1);
  }

  mprotect((void*)(((unsigned long)readString)&0xfffffffffffff000), 1,
           PROT_NONE);
}
